# casc_testathon_ManriqueGarcia


## Description

This repository give us a place to maintain a control versions of the every object in an Ansible Automation Controller.

CasC (Configuration as Code)  means the posibility of define every object of Ansible Automation Controller as code in a git repository. In this lab, we have defined two environments (dev and pro) to do the CasC and interact with a gitops approach between them.

## Steps to test the CasC approach
### Day-Zero from CLI

Before using CasC as a GitOps approach, it is needed to launch an initialization from CLI which it is called Day-Zero.

1. Clone the repository and create a new day-zero branch

   ```
   git clone git@gitlab.com:CasC-Testathon/20221130/casc_testathon_ManriqueGarcia.git
   cd casc_testathon_ManriqueGarcia/
   git checkout -b casc-dev-day0
   ```

2. Add initial Production release version number
   ```
   cat <<EOF > VERSION
   0.1
   EOF
   ```

3. Edit credentials to connect to the controller for day zero.

   ```
   vi group_vars/dev/configure_connection_controller_credentials.yml
   vi group_vars/pro/configure_connection_controller_credentials.yml
   ansible-vault encrypt group_vars/dev/configure_connection_controller_credentials.yml group_vars/pro/configure_connection_controller_credentials.yml
   ```

4. Edit credential objects for day zero.

   Automation Hub Token: b7d2b62539f8cd6d42f035985cc0e039078a988f\
   Vault Password: Your choice.\
   Gitlab Credential: Your SSH KEY for gitlab.\
   GitLab API Token: Generate a Token for your user:
     1. Go to gitlab.com web and login with your user.
     2. Right top, click profile icon and "Edit Profile"
     3. Click Access Tokens
     4. Select next day for Expiration Day and CasC-testathon as Token name.
     5. Select scope Api
     6. Save generated token.


   ```
   vi orgs_vars/casc_testathon_ManriqueGarcia/env/dev/controller_credentials.d/controller_credentials.yml
   vi orgs_vars/casc_testathon_ManriqueGarcia/env/pro/controller_credentials.d/controller_credentials.yml
   ansible-vault encrypt orgs_vars/casc_testathon_ManriqueGarcia/env/pro/controller_credentials.d/* orgs_vars/casc_testathon_ManriqueGarcia/env/dev/controller_credentials.d/*
   ```

5. Check the inventory file. Example:

   ```
   [dev]
   controller-dev-testathon-ansible-automation-platform.apps.cluster-2drjc.2drjc.sandbox2634.opentlc.com

   [pro]
   controller-prd-testathon-ansible-automation-platform.apps.cluster-2drjc.2drjc.sandbox2634.opentlc.com
   ```

6. Setting vault credential file. It should be the same you configured in step 3.

   ```
   echo "my_vault_pass" > ~/.vault_password
   ln ~/.vault_password .
   ```

7. Launch ansible-navigator from CLI to setup day-zero of CasC. Example:

   ```
   ansible-navigator run casc_ctrl_config.yml -i inventory -l dev -e '{orgs: casc_testathon_ManriqueGarcia, dir_orgs_vars: orgs_vars, env: dev}' -m stdout --eei quay.io/automationiberia/aap/ee-casc --vault-password-file .vault_password
   ansible-navigator run casc_ctrl_config.yml -i inventory -l pro -e '{orgs: casc_testathon_ManriqueGarcia, dir_orgs_vars: orgs_vars, env: pro}' -m stdout --eei quay.io/automationiberia/aap/ee-casc --vault-password-file .vault_password
   ```

8. Push the changes

   ```
   git status -s
   git add -A
   git commit -m "CasC day zero"
   git push origin casc-dev-day0
   ```

9. Pomote the `casc-dev-day0` branch to dev (`dev` branch)

   * Select the source branch as `casc-dev-day0` and `dev` as the destination one]
     ![New Merge Request Dev](images/newmrtodev-0.png "New Merge Request Dev")

   * Fill in the merge request information
     ![Fill in the merge request information](images/newmrtodev-0-step2.png "Fill in the merge request information")

   * Approve the Merge Request.
     ![New Merge Request Pro](images/newmrtodev-0-step3.png "Merge the merge request")

10. Promote the `dev` branch to pro (`pro` branch)

   * Select the source branch as `dev` and `pro` as the destination one]
     ![New Merge Request Pro](images/newmrtopro.png "New Merge Request Pro")

   * Fill in the merge request information
     ![Fill in the merge request information](images/newmrtoprostep2.png "Fill in the merge request information")

     > :warning: **Be sure to write a title that have sense for the Merge Request**: The default value here is `dev`, that is not usefull at all!

   * Approve the Merge Request.
     ![New Merge Request Pro](images/newmrtoprostep3.png "Merge the merge request")

11. Configure the webhooks for both environments: DEV and PRO.

   * Launch de JT casc_testathon_ManriqueGarcia JT_Gitlab_Webhook_Creation in controller-dev-testathon-ansible-automation-platform.apps.cluster-2drjc.2drjc.sandbox2634.opentlc.com

   * Launch de JT casc_testathon_ManriqueGarcia JT_Gitlab_Webhook_Creation in controller-prd-testathon-ansible-automation-platform.apps.cluster-2drjc.2drjc.sandbox2634.opentlc.com

   Webhooks should be created in the casc_testathon_ManriqueGarcia gitlab project. Check them.


### GitOps flow

1. Clone the given repository:

   ```
   git clone git@gitlab.com:CasC-Testathon/20221130/casc_testathon_ManriqueGarcia.git

   cd casc_testathon_ManriqueGarcia/
   ```

2. Create a new branch from `dev` to introduce the new items:

   ```
   git checkout dev
   git pull
   git checkout -b add_info_job_template
   ```

3. Add a new Playbook and a new Job Template

   File: `new_playbook1.yaml`
   ```
   cat > new_playbook1.yaml <<EOF
   ---
   - name: "Play to show the hostname"
     hosts: all
     tasks:
       - name: "Show the hostname"
         debug:
           msg:
             - "This server is called (from Ansible inventory):     {{ inventory_hostname }}"
             - "This server is called (from Execution Environment): {{ lookup('pipe', 'cat /etc/hostname') }}"
             - "Running as user: {{ lookup('pipe', 'id') }}"
   ...
   EOF
   ```

   File: `new_playbook2.yaml`
   ```
   cat > new_playbook2.yaml <<EOF
   ---
   - name: "Play to show the hostname"
     hosts: all
     connection: local
     tasks:
       - name: "Show the hostname"
         debug:
           msg:
             - "This server is called (from Ansible inventory):     {{ inventory_hostname }}"
             - "This server is called (from Execution Environment): {{ lookup('pipe', 'cat /etc/hostname') }}"
             - "Running as user: {{ lookup('pipe', 'id') }}"
   ...
   EOF
   ```

   File: `orgs_vars/casc_testathon_ManriqueGarcia/env/common/controller_job_templates.d/new_job_template.yaml`
   ```
   cat > orgs_vars/casc_testathon_ManriqueGarcia/env/common/controller_job_templates.d/new_job_template.yaml <<EOF
   ---
   controller_templates:
     - name: "{{ orgs }} New Job Template"
       description: "Template to show how to add a new JT"
       organization: "{{ orgs }}"
       project: "{{ orgs }} CasC_Data"
       inventory: "{{ orgs }} Localhost"
       playbook: "new_playbook1.yaml"
       job_type: run
       fact_caching_enabled: false
       concurrent_jobs_enabled: true
       ask_scm_branch_on_launch: false
       extra_vars:
         ansible_python_interpreter: /usr/bin/python3
         ansible_async_dir: /home/runner/.ansible_async/
       execution_environment: "ee-casc"
   ...
   EOF
   ```

4. Modify Production release version number file
   ```
   cat <<EOF > VERSION
   0.2
   EOF
   ```

5. Commit the changes to the new branch

   ```
   git add -A .
   git commit -am "Add new playbook and job template to show server information"
   git push -u origin add_info_job_template
   ```

6. Create a Merge Request to `dev` branch <a name="mrtodev"></a>

   * Go to Merge Requests and create a new merge request
     ![New merge request](images/mrtodev.png "New merge request")

   * Select the source branch `add_info_job_template` and `dev` as the destination one
     ![New merge request to dev](images/newmrtodev.png "New merge request to dev")

   * Fill in the merge request information
     ![Fill in the merge request information](images/newmrtodevstep2.png "Fill in the merge request information")

   * Merge the merge request
     ![Merge the merge request](images/newmrtodevstep3.png "Merge the merge request")

   ---

   The following automated process has ben executed at the Ansible Automation Controller:

   ![Look at the Ansible Automation Controller's jobs](images/devworkflowjobs.png "Look at the Ansible Automation Controller's jobs")

   The following diagram shows the components of the workflow:

   ![Workflow Diagram](images/workflowdiagram.png "Workflow Diagram")

   Of course, the new Job Template has been created:

   ![New Job Template Dev](images/newjtdev.png "New Job Template Dev")

7. Pomote the `dev` branch to production (`pro` branch) <a name="mrdevtopro"></a>

   Similarly to the [step 5](#mrtodev), create a new Merge Request from the `dev` branch to the `pro` branch:

   * Select the source branch as `dev` and `pro` as the destination one]
     ![New Merge Request Pro](images/newmrtopro.png "New Merge Request Pro")

   * Fill in the merge request information
     ![Fill in the merge request information](images/newmrtoprostep2.png "Fill in the merge request information")

     > :warning: **Be sure to write a title that have sense for the Merge Request**: The default value here is `dev`, that is not usefull at all!

   ---

   When the Merge Request is already merged, the new Job Template is also created in the `pro` environment:

   ![New Job Template Pro](images/newjtpro.png "New Job Template Pro")

8. Run the new Job Template at PRO <a name="runjtpro"></a>

   Run the Job Template:
   ![Run the JT](images/launchjtpro.png "Run the JT")

   and check that it is failing:
   ![Job Template PRO failed](images/jtprofailed.png "Job Template PRO failed")

9. Rollback the PRO environment to previously working tag

   To rollback the status of the controller to a previous working version, it's only needed to run the following Job Templates:

   * Run the Job Template `casc_testathon_ManriqueGarcia CasC_Ctrl_Drop_Diff` with the previous working version:
     ![Run the JT casc_testathon_ManriqueGarcia CasC_Ctrl_Drop_Diff](images/rollbackddv0.2.png "Run the JT casc_testathon_ManriqueGarcia CasC_Ctrl_Drop_Diff")
   * Run the Job Template `casc_testathon_ManriqueGarcia JT_CasC_Ctrl_Config` with the previous working version:
     ![Run the JT casc_testathon_ManriqueGarcia JT_CasC_Ctrl_Config](images/rollbackccv0.2.png "Run the JT casc_testathon_ManriqueGarcia JT_CasC_Ctrl_Config")

10. Fix your playbook

   ```
   git checkout dev
   git pull
   git checkout -b fix_playbook
   ```

   Modify the Job Template to use the correct playbook:

   `playbook: "new_playbook2.yaml"`

   Updated file: `orgs_vars/casc_testathon_ManriqueGarcia/env/common/controller_job_templates.d/new_job_template.yaml`
   ```yaml
   ---
   controller_templates:
     - name: "{{ orgs }} New Job Template"
       description: "Template to show how to add a new JT"
       organization: "{{ orgs }}"
       project: "{{ orgs }} CasC_Data"
       inventory: "{{ orgs }} Localhost"
       playbook: "new_playbook2.yaml"
       job_type: run
       fact_caching_enabled: false
       concurrent_jobs_enabled: true
       ask_scm_branch_on_launch: true
       extra_vars:
         ansible_python_interpreter: /usr/bin/python3
         ansible_async_dir: /home/runner/.ansible_async/
       execution_environment: "ee-casc"
   ...
   ```

  Modify Production release version number file
   ```
   cat <<EOF > VERSION
   0.3
   EOF
   ```

   Commit and push your changes:

   ```
   git commit -am "Fix the connection method"
   git push -u origin fix_playbook
   ```

   Create a Merge Request to `dev` branch:

   * ![Create New Merge Request from `fix_playbook` to Dev](images/newmrtodev-fix_playbook.png "Create New Merge Request from `fix_playbook` to Dev")
   * ![New Merge Request from `fix_playbook` to Dev](images/newmrtodev-fix_playbook-step2.png "New Merge Request from `fix_playbook` to Dev")
   * ![Merge the Request](images/newmrtodev-fix_playbook-step3.png "Merge the Request")

   Repeat the steps to create a new Merge Request from `dev` to `pro`, as described at [step 6](#mrdevtopro)

10. Run again the new Job Template at PRO <a name="runjtpro"></a>

    Run again the Job Template:
    ![Run the JT](images/launchjtpro.png "Run the JT")

    and check that it is working fine now:
    ![Job Template PRO failed](images/jtprosuccess.png "Job Template PRO failed")
